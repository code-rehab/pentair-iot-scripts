import { readFileSync } from "fs";

import { ProcessParams, ShellCommand, ShellCommands } from "../classes/command-executor";
import { AWSContainer } from "../components/aws/aws";
import { requireProps } from "../helpers";

export interface AWSCommands {
  s3: ShellCommands<{
    pull: ShellCommand;
    push: ShellCommand;
  }>;

  cognito: ShellCommands<{
    createUsers: ShellCommand;
    removeUsers: ShellCommand;
  }>;

  appsync: ShellCommands<{
    build: ShellCommand;
  }>;
}

function getConfig(path_to_config: string) {
  requireProps({ path_to_config });

  return JSON.parse(readFileSync(process.env.PWD + path_to_config.replace("./", "/"), "utf8"));
}

export function awsCommands(): AWSCommands {
  const aws = new AWSContainer();

  return {
    // s3 commands
    s3: {
      // push command for uploading changes on a s3 bucket
      push: async ({ bucketRef, source, location, config }: ProcessParams) =>
        config ? aws.s3.pushAll(getConfig(config)) : aws.s3.push(bucketRef, source, location),

      // pull command for downloading changes on a s3 bucket
      pull: async ({ bucketRef, source, location, config }: ProcessParams) =>
        config ? aws.s3.pullAll(getConfig(config)) : aws.s3.pull(bucketRef, source, location)
    },

    // cognito commands
    cognito: {
      createUsers: async ({ config }: ProcessParams) => aws.cognito.createUsers(getConfig(config)),
      removeUsers: async ({ config }: ProcessParams) => aws.cognito.removeUsers(getConfig(config))
    },
    appsync: {
      build: async (params: ProcessParams) => aws.appsync.build(params)
    }
  };
}
