export const PWD = process.env.PWD;
export const build_path = PWD + "/.build/";

export { aws_config } from "./aws";
