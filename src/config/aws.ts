import { build_path, PWD } from "./defaults";

export interface AppsyncConfig {
  schema: string;
  build_path: string;
}

export const aws_config = {
  appsync: {
    build_path: build_path + "appsync/",
    schema: PWD + "/schema.graphql"
  }
};
