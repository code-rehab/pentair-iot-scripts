import { logger } from './classes/logger';

export function requireProps(
  props: { [key: string]: any },
  message: string = "Please specify the missing arguments and try again",
  exitOnError: boolean = true
) {
  let error = false;
  Object.keys(props).forEach((key: string) => {
    if (props[key] === undefined) {
      error = true;
      message = `${key} is required\n${message}`;
    }
  });

  if (error) {
    logger.error(`${message}`);

    if (exitOnError) {
      process.exit(1);
    }
  }
}
