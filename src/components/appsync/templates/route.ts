import { AppsyncRouteConfig } from "../appsync-data";
import { AppsyncTemplate } from "../appsync";

export const route_templates: AppsyncTemplate = {
  request: {
    resolve: ({ route }: AppsyncRouteConfig) => `
    #**
      The value of 'payload' after the template has been evaluated
      will be passed as the event to AWS Lambda.
    *#
    {
        "version" : "2017-02-28",
        "operation": "Invoke",
        "payload": {
            "route": "${route}",
            "arguments": $utils.toJson($context.arguments),
            "identity": $utils.toJson($context.identity)
        }
    }`,
    resolveBatch: ({ route, field }: AppsyncRouteConfig) => `
    #**
      The value of 'payload' after the template has been evaluated
      will be passed as the event to AWS Lambda.
    *#
    {
        "version" : "2017-02-28",
        "operation": "BatchInvoke",
        "payload": {
            "route": "${route}",
            "field": "${field}",
            "source": $utils.toJson($context.source),
            "arguments": $utils.toJson($context.arguments),
            "identity": $utils.toJson($context.identity)
        }
    }`
  },

  response: {
    resolve: () => `$utils.toJson($context.result)`,
    resolveBatch: () => `$utils.toJson($context.result)`
  }
};
