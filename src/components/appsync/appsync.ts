import { writeFileSync, readFileSync } from "fs";
import { emptyDirSync } from "fs-extra";
import { importSchema } from "graphql-import";
import { makeExecutableSchema, transformSchema } from "graphql-tools";

import { logger } from "../../classes/logger";
import { AppsyncConfig, aws_config } from "../../config/aws";
import { AWSContainer } from "../aws/aws";
import { printSchema } from "graphql";
import { ModelDirective } from "./directives/model";
import { RouteDirective } from "./directives/route";
import { schemaData, AppsyncRouteConfig } from "./appsync-data";
import { route_templates } from "./templates/route";

export const mapping: any = [];

export type AppsyncTemplateFunction = Record<string, (params: AppsyncRouteConfig) => string>;
export type AppsyncTemplate = Record<"response" | "request", AppsyncTemplateFunction>;
export type AppsyncTemplates = Record<string, AppsyncTemplate>;

export class AWS_Appsync {
  public config: AppsyncConfig;
  private _extractedSubscriptionsBlock: string = "";
  private _extractedSubscriptions: string = "";
  private _subscriptionRegex = /type Subscription \{([^}]+)\}/gm;
  private _templates: AppsyncTemplates = {
    route: route_templates
  };

  constructor(private _aws?: AWSContainer, config: Partial<AppsyncConfig> = {}) {
    this.config = this.createConfig(config);
  }

  public async build(config: Partial<AppsyncConfig> = {}) {
    this.config = this.createConfig(config);

    try {
      await this.createSchema();
      await this.generateMaps();
      // TODO: Add function for Generating python routes.

      logger.info("Build: Finished compiling Appsync dependencies");
    } catch (e) {
      logger.error(e);
    }
  }

  public createConfig = (config: Partial<AppsyncConfig> = {}) => {
    return {
      ...aws_config.appsync,
      ...config
    };
  };

  private _formatFileName(filename: string, suffix: string): string {
    return (filename + suffix).toLowerCase().replace(".", "-");
  }

  public async generateMaps() {
    logger.info("Build: Generating Appsync resolver mappings");
    const build_path = this.config.build_path;
    const mapping_build_path = this.config.build_path + "mapping/";
    const mapping = schemaData.mapping.get();

    emptyDirSync(mapping_build_path);
    const mapConfig = [];

    for (const key in mapping) {
      const cfg = mapping[key];
      const fields = key.split(".");
      const requestFileName = this._formatFileName(key, "-request.txt");
      const responseFileName = this._formatFileName(key, "-response.txt");

      const templates = this.generateTemplates(cfg);

      writeFileSync(mapping_build_path + requestFileName, templates.request);
      writeFileSync(mapping_build_path + responseFileName, templates.response);

      mapConfig.push({
        dataSource: "AppSyncResolver",
        type: fields[0],
        field: fields[1],
        request: requestFileName,
        response: responseFileName
      });
    }

    writeFileSync(build_path + "mapping.json", JSON.stringify(mapConfig));
    return mapConfig;
  }

  public generateTemplates(cfg: AppsyncRouteConfig) {
    const request = this._templates[cfg.template.type]["request"][cfg.template.use];
    const response = this._templates[cfg.template.type]["response"][cfg.template.use];

    return {
      request: request(cfg),
      response: response(cfg)
    };
  }

  public createSchema(_schema?: string) {
    logger.info("Build: Stitching Appsync schema");
    const root = ` 
    schema {
      mutation: Mutation
      query: Query
      subscription: Subscription
    }`.trim();

    const pentair = `
    ${ModelDirective.typeDef()}
    ${RouteDirective.typeDef()}

    scalar AWSJSON
    directive @aws_subscribe(mutations: [String!]!) on FIELD_DEFINITION`;

    const schema = importSchema(_schema || this.config.schema, {
      pentair
    });

    const typeDefs = root + "\n\n" + schema;
    const executableSchema = makeExecutableSchema({
      typeDefs,
      schemaDirectives: {
        route: RouteDirective,
        model: ModelDirective
      }
    });

    const match = this._subscriptionRegex.exec(typeDefs);
    this._extractedSubscriptions = (match && match[1]) || "";

    const buildPath = this.config.build_path + "schema.graphql";
    const transformedSchema = transformSchema(executableSchema, [ModelDirective.transformer()]);

    let finalSchema = printSchema(transformedSchema);

    finalSchema = finalSchema.replace(ModelDirective.typeDef() + "\n", "");
    finalSchema = finalSchema.replace(RouteDirective.typeDef() + "\n", "");
    finalSchema = finalSchema.replace("scalar AWSJSON", "");
    finalSchema = finalSchema.replace("scalar AWSJSON", "");
    finalSchema = this.appendSubscriptions(finalSchema);
    finalSchema = finalSchema.trim();

    try {
      writeFileSync(buildPath, finalSchema);
    } catch (e) {
      logger.error("Unable to write schema to: " + buildPath);
    }

    return finalSchema;
  }

  appendSubscriptions = (typeDefs: string) => {
    let subscriptions = this._extractedSubscriptions.trim();

    schemaData.models.toArray().forEach(model => {
      const typename = model.name;
      subscriptions += `
  ${typename.toLowerCase()}Updated: ${typename} @aws_subscribe(mutations: ["update${typename}"])
  ${typename.toLowerCase()}Deleted: ${typename} @aws_subscribe(mutations: ["delete${typename}"])
  ${typename.toLowerCase()}Created: ${typename} @aws_subscribe(mutations: ["create${typename}"])
`;
    });

    const match = /type Subscription \{([^}]+)\}/gm.exec(typeDefs);
    return typeDefs.replace((match && match[1]) || "", "\n  " + subscriptions);
  };
}
