import { SchemaDirectiveVisitor } from "graphql-tools";
import { GraphQLField, GraphQLObjectType, GraphQLInterfaceType, assertListType, isListType } from "graphql";
import { schemaData } from "../appsync-data";
import { getFieldDef } from "graphql/execution/execute";

type GraphQLFieldDetails = {
  objectType: GraphQLObjectType | GraphQLInterfaceType;
};

export class RouteDirective extends SchemaDirectiveVisitor {
  public static typeDef() {
    return "directive @route(to: String) on FIELD_DEFINITION";
  }

  public visitFieldDefinition(field: GraphQLField<any, any>, details: GraphQLFieldDetails) {
    const typeName = details.objectType.name;
    const fieldName = field.name;
    const template = { type: "route", use: "resolveBatch" };

    schemaData.mapping.insert(typeName + "." + fieldName, {
      template,
      route: this.args.to || typeName.toLowerCase() + "." + fieldName,
      field: field.name,
      type: details.objectType
    });
  }
}
