import { SchemaDirectiveVisitor, Transform } from "graphql-tools";
import { GraphQLObjectType, GraphQLSchema, GraphQLID } from "graphql";
import { schemaData, AppsyncRouteConfig } from "../appsync-data";
import { DefaultTransformer } from "../classes/transformer";

enum ResourceFunction {
  Get = "get",
  List = "list",
  Create = "create",
  Update = "update",
  Delete = "delete"
}

export class ModelDirective extends SchemaDirectiveVisitor {
  public static typeDef() {
    return `directive @model(queries: String) on OBJECT`;
  }

  public static transformer() {
    return new ModelTransformer();
  }

  public createRoute(functionName: ResourceFunction, type: GraphQLObjectType): AppsyncRouteConfig {
    return {
      route: type.name.toLowerCase() + "." + functionName,
      template: { type: "route", use: "resolve" },
      field: "",
      type: type
    };
  }

  public visitObject(object: GraphQLObjectType) {
    const name = object.name;

    schemaData.models.insert(name, object);
    schemaData.mapping
      .insert("Query." + name, this.createRoute(ResourceFunction.Get, object))
      .insert("Query." + name + "Collection", this.createRoute(ResourceFunction.List, object))
      .insert("Mutation.create" + name, this.createRoute(ResourceFunction.Create, object))
      .insert("Mutation.update" + name, this.createRoute(ResourceFunction.Update, object))
      .insert("Mutation.delete" + name, this.createRoute(ResourceFunction.Delete, object));
  }
}

class ModelTransformer extends DefaultTransformer implements Transform {
  public transformSchema(schema: GraphQLSchema) {
    this._schema = schema;
    this._config = schema.toConfig();

    let mutationFields = {};
    let queryFields: Record<string, any> = {};

    schemaData.models.toArray().forEach(model => {
      const name = model.name;

      if (!this._config!.query!.getFields()[name]) {
        queryFields[name] = { type: name, arguments: { id: { type: GraphQLID, required: true } } };
      }

      if (!this._config!.query!.getFields()["all" + name + "s"]) {
        queryFields[name + "Collection"] = { type: name, list: true };
      }

      mutationFields = {
        ["create" + name]: { type: name, arguments: { input: { type: "input" + name, required: true } } },
        ["update" + name]: { type: name, arguments: { input: { type: "input" + name, required: true } } },
        ["delete" + name]: { type: name, arguments: { id: { type: GraphQLID, required: true } } },
        ...mutationFields
      };
    });

    this.addFields("Mutation", mutationFields);
    this.addFields("Query", queryFields);

    return new GraphQLSchema(this._config);
  }
}
