import { makeExecutableSchema } from "graphql-tools";
import { schemaData } from "../../appsync-data";
import { RouteDirective } from "../route";

const typeDefs = RouteDirective.typeDef();

test("@route should add a route to schemaData", () => {
  makeExecutableSchema({
    typeDefs:
      typeDefs +
      `  
      type User {
        name: String,
        description: String @route
        someList: [String] @route
      }
      `,
    schemaDirectives: {
      route: RouteDirective
    }
  });

  const result = schemaData.mapping.get();
  expect(JSON.stringify(result["User.description"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"user.description","field":"description","type":"User"}'
  );

  expect(JSON.stringify(result["User.someList"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"user.someList","field":"someList","type":"User"}'
  );
});

test("@route(to:String) should add a route to schemaData with the defined route", () => {
  makeExecutableSchema({
    typeDefs:
      typeDefs +
      `  
      type User {
        name: String,
        description: String @route(to:"some.route.name")
      }
      `,
    schemaDirectives: {
      route: RouteDirective
    }
  });

  const result = schemaData.mapping.get();
  expect(JSON.stringify(result["User.description"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"some.route.name","field":"description","type":"User"}'
  );
});
