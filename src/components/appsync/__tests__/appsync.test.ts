import { AWS_Appsync } from "../appsync";
import { logger } from "../../../classes/logger";
import { GraphQLObjectType } from "graphql";

logger.silent = true;

describe("Appsync tests", () => {
  test("Should return a valid appsync schema", () => {
    const appsync = new AWS_Appsync();
    const schema = appsync.createSchema(`
    # import * from "pentair"

    input inputUser {
      name: String
      description: String
    }

    type User @model {
      name: String
      description: String @route(to:"some.route") 
    }
 
    type Query{ 
      User(id:ID!): User 
    }
    type Mutation
    type Subscription {
      subscriptionToExtract: User @aws_subscribe(mutations: ["updateUser"])
    }
  `);

    expect(schema.replace(/\s/g, "")).toBe(
      `
     input inputUser {
       name: String
       description: String
     }

     type Mutation {
       createUser(input: inputUser!): User
       updateUser(input: inputUser!): User
       deleteUser(id: ID!): User
     } 

     type Query {
       User(id: ID!): User
       UserCollection: [User]
     }

     type Subscription {
       subscriptionToExtract: User @aws_subscribe(mutations: ["updateUser"])
       userUpdated: User @aws_subscribe(mutations: ["updateUser"])
       userDeleted: User @aws_subscribe(mutations: ["deleteUser"])
       userCreated: User @aws_subscribe(mutations: ["createUser"])
     }

     type User {
       name: String
       description: String
     }`.replace(/\s/g, "")
    );
  });

  test("Should be able to generate correct templates", () => {
    const appsync = new AWS_Appsync();

    const resolve = appsync.generateTemplates({
      template: { type: "route", use: "resolve" },
      field: "SomeField",
      route: "some.route",
      type: new GraphQLObjectType({
        name: "SomeType",
        fields: {}
      })
    });

    const resolveBatch = appsync.generateTemplates({
      template: { type: "route", use: "resolveBatch" },
      field: "SomeField",
      route: "some.route",
      type: new GraphQLObjectType({
        name: "SomeType",
        fields: {}
      })
    });

    expect(resolve).toEqual({
      request: `
    #**
      The value of 'payload' after the template has been evaluated
      will be passed as the event to AWS Lambda.
    *#
    {
        "version" : "2017-02-28",
        "operation": "Invoke",
        "payload": {
            "route": "some.route",
            "arguments": $utils.toJson($context.arguments),
            "identity": $utils.toJson($context.identity)
        }
    }`,
      response: "$utils.toJson($context.result)"
    });

    expect(resolveBatch).toEqual({
      request: `
    #**
      The value of 'payload' after the template has been evaluated
      will be passed as the event to AWS Lambda.
    *#
    {
        "version" : "2017-02-28",
        "operation": "BatchInvoke",
        "payload": {
            "route": "some.route",
            "field": "SomeField",
            "source": $utils.toJson($context.source),
            "arguments": $utils.toJson($context.arguments),
            "identity": $utils.toJson($context.identity)
        }
    }`,
      response: "$utils.toJson($context.result)"
    });
  });
});
