import { GraphQLObjectType, GraphQLInterfaceType } from "graphql";

export type AppsyncRouteMapping = Record<string, AppsyncRouteConfig>;

export class AppsyncObjectController<T> implements IAppsyncObjectController<T> {
  private _data: Record<string, T> = {};

  public get(): Record<string, T> {
    return this._data;
  }

  public keys(): string[] {
    return Object.keys(this._data);
  }

  public toArray(): T[] {
    return this.keys().map(key => this._data[key]);
  }

  public insert(key: string, value: T) {
    this._data[key] = value;
    return this;
  }
}

class AppsyncSchemaData {
  public mapping: AppsyncMappingController = new AppsyncObjectController<AppsyncRouteConfig>();
  public models: AppsyncModelsController = new AppsyncObjectController<GraphQLObjectType>();
}

export interface IAppsyncObjectController<T> {
  insert(key: string, value: T): AppsyncObjectController<T>;
  keys(): string[];
  toArray(): T[];
  get(): Record<string, T>;
}

export interface AppsyncRouteConfig {
  route: string;
  template: { type: string; use: string };
  type: GraphQLObjectType | GraphQLInterfaceType;
  field: string;
}

export type AppsyncMappingController = IAppsyncObjectController<AppsyncRouteConfig>;
export type AppsyncModelsController = IAppsyncObjectController<GraphQLObjectType>;
export const schemaData = new AppsyncSchemaData();
