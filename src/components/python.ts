import archiver from "archiver";
import { spawn } from "child_process";
import { copy, createWriteStream, existsSync, mkdirSync, removeSync } from "fs-extra";

import { logger } from "../classes/logger";
import { build_path } from "../config/defaults";

export class Python {
  private _config: any = {};

  constructor(config: any) {
    // this.config = cfg.create(config);
  }

  async buildDev(source: string, destination: string = build_path + "python/", artifact_name = "lambda") {
    // destination = destination || this.config.build_path;
    await this.build_source(source, destination);
    await this.create_artifact(destination, artifact_name);
  }

  async build(
    source: string,
    destination: string = build_path + "python/",
    name: string = "lambda",
    dependencies: boolean
  ) {
    removeSync(destination + name);

    if (dependencies) {
      await this.build_dependencies(destination + name);
      logger.info("Build: Finished building dependencies for: " + name);
    }

    if (source) {
      await this.build_source(source, destination + name);
      await this.create_artifact(destination + name, name);
      logger.info("Build: Finished building source for: " + name);
    }
  }

  async format() {
    logger.info("Formatting Python files...");
    return new Promise((resolve, reject) => {
      const child = spawn("pipenv run black .*/**/*.py", {
        shell: "/bin/bash"
      });
      child.on("close", function(code) {
        if (code !== 0) {
          logger.error(`Command failed with exit code ${code}.`);
          process.exit(1);
        }
        resolve();
      });
      // print live stdout data to the console
      child.stdout.on("data", data => {
        process.stdout.write(`${data}`);
      });
      // print live stderr data to the console
      child.stderr.on("data", data => {
        logger.error(`${data}`);
      });
    });
  }

  public async test_source() {
    logger.info("Running Python tests...");
    return new Promise((resolve, reject) => {
      const child = spawn("pipenv run pytest", { shell: "/bin/bash" });
      child.on("close", function(code) {
        if (code !== 0) {
          logger.error(`Command failed with exit code ${code}.`);
          process.exit(1);
        }
        resolve();
      });
      // print live stdout data to the console
      child.stdout.on("data", data => {
        process.stdout.write(`${data}`);
      });
      // print live stderr data to the console
      child.stderr.on("data", data => {
        logger.error(`${data}`);
      });
    });
  }

  public async build_dependencies(destination?: string) {
    logger.info("Build: Fetching python dependencies...");
    return new Promise((resolve, reject) => {
      const child = spawn(`pip install --upgrade -r <(pipenv lock -r) --target ${destination}`, { shell: "/bin/bash" });
      child.on("close", function(code) {
        if (code !== 0) {
          logger.error(`Command failed with exit code ${code}.`);
          process.exit(1);
        }
        logger.info(`Build: Python dependencies copied to ${destination}`);
        resolve();
      });
      // print live stdout data to the console
      child.stdout.on("data", data => {
        process.stdout.write(`${data}`);
      });
      // print live stderr data to the console
      child.stderr.on("data", data => {
        process.stdout.write(`${data}`);
      });
    });
  }

  public async build_source(source: string, destination: string) {
    logger.info("Build: Copying source files...");
    return new Promise((resolve, reject) => {
      copy(source, destination)
        .then(() => {
          logger.info("Build: Successfully copied files");
          resolve();
        })
        .catch((err: any) => {
          logger.error(`Build: Error copying files: ${err}`);
          process.exit(1);
          reject();
        });
    });
  }

  public async create_artifact(source: string, name: string) {
    logger.info("Build: Creating artifact...");
    if (!existsSync(".build/artifacts")) {
      mkdirSync(".build/artifacts");
    }

    return new Promise((resolve, reject) => {
      var output = createWriteStream(".build/artifacts/" + name + ".zip");
      var archive = archiver("zip", {
        zlib: { level: 9 } // Sets the compression level.
      });
      output.on("close", function() {
        logger.info("Build: Successfully created artifact " + name);
        resolve();
      });
      archive.on("error", function(err) {
        reject(err);
      });
      archive.directory(source, false);
      archive.pipe(output);
      archive.finalize();
    });
  }
}
