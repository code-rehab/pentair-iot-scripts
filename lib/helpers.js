"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./classes/logger");
function requireProps(props, message, exitOnError) {
  if (message === void 0) {
    message = "Please specify the missing arguments and try again";
  }
  if (exitOnError === void 0) {
    exitOnError = true;
  }
  var error = false;
  Object.keys(props).forEach(function(key) {
    if (props[key] === undefined) {
      error = true;
      message = key + " is required\n" + message;
    }
  });
  if (error) {
    logger_1.logger.error("" + message);
    if (exitOnError) {
      process.exit(1);
    }
  }
}
exports.requireProps = requireProps;
