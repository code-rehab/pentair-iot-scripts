import { ShellCommands } from "./classes/command-executor";
import { AWSContainer } from "./components/aws/aws";
import { Python } from "./components/python";
import { AWS_Appsync } from "./components/appsync/appsync";
import { Env } from "./components/env";
export declare function routine(routineFunction: () => Promise<void>): Promise<void>;
export { config as defaultconfig } from "./config";
export { logger } from "./classes/logger";
declare class Module {
    private _python;
    readonly python: Python;
    private _env;
    readonly env: Env;
    private _appsync;
    readonly appsync: AWS_Appsync;
    private _aws;
    readonly aws: AWSContainer;
}
export declare function commands(commands: ShellCommands<any>): void;
declare const module: Module;
export default module;
