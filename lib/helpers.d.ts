export declare function requireProps(props: {
    [key: string]: any;
}, message?: string, exitOnError?: boolean): void;
