import { AWSCommands } from './commands/aws';
import { PythonCommands } from './commands/python';
export interface PentairCommands {
    python: PythonCommands;
    aws: AWSCommands;
}
export declare const python: PythonCommands;
export declare const aws: AWSCommands;
