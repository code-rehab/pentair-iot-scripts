"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
  logger: {
    level: process.env.LOG_LEVEL || "info"
  },
  python: {
    build_path: "./build",
    source: "./py"
  },
  aws: {
    s3: {
      source: "./aws/s3",
      prefix: ""
    }
  }
};
