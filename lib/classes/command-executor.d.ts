export declare type ProcessParams = Record<string, string>;
export declare type ProcessCommandNames = string[];
export declare type ProcessCommandResponse = Promise<void | string | undefined>;
export declare type ShellCommands<CommandNames> = {
    [name in keyof CommandNames]: ShellCommand | ShellCommands<string>;
};
export declare type ShellCommand = (params: ProcessParams, commands: ProcessCommandNames) => ProcessCommandResponse;
export declare class CommandExecutor {
    private _commands;
    private _args;
    private _params;
    private _processCommands;
    private _currentCommand;
    constructor(_commands: ShellCommands<any>, _args?: string[]);
    run: () => void;
    private _findCommand;
    private _parseArgs;
    commandUnknown: (_params: Record<string, string>, _processCommands: string[]) => void;
    commandRequired: (commands: ShellCommands<any>) => (_params: Record<string, string>, _processCommands: string[]) => Promise<void>;
    printResponse: (command?: Promise<string | void | undefined> | undefined) => Promise<void>;
}
