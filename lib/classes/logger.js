"use strict";
var __importStar =
  (this && this.__importStar) ||
  function(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null)
      for (var k in mod)
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
  };
Object.defineProperty(exports, "__esModule", { value: true });
var winston = __importStar(require("winston"));
var config_1 = require("../config");
var _a = winston.format,
  combine = _a.combine,
  simple = _a.simple,
  colorize = _a.colorize,
  printf = _a.printf;
var level = config_1.config.logger.level;
exports.logger = winston.createLogger({
  level: level,
  format: combine(
    simple(),
    colorize({ all: true }),
    printf(function(_a) {
      var message = _a.message,
        level = _a.level;
      return level === "error" ? "\n" + message + "\n" : "" + message;
    })
  ),
  defaultMeta: undefined,
  transports: [new winston.transports.Console()]
});
