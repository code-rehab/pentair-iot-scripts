export interface AppsyncConfig {
    schema: string;
    build_path: string;
}
export declare const aws_config: {
    appsync: {
        build_path: string;
        schema: string;
    };
};
