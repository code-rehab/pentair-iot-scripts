"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var defaults_1 = require("./defaults");
exports.aws_config = {
  appsync: {
    build_path: defaults_1.build_path + "appsync/",
    schema: defaults_1.PWD + "/schema.graphql"
  }
};
