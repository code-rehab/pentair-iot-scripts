import { AWSContainer } from "./aws/aws";
interface EnvCreateConfig {
    file: string;
    values: Record<string, string>;
}
export declare class Env {
    config: EnvCreateConfig;
    private _aws;
    readonly aws: AWSContainer;
    create(config: EnvCreateConfig): void;
    private fetchValues;
}
export {};
