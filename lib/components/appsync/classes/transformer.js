"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var DefaultTransformer = /** @class */ (function() {
  function DefaultTransformer() {}
  DefaultTransformer.prototype.pluckType = function(typename) {
    if (this._config) {
      return this._config.types.splice(
        this._config.types.findIndex(function(type) {
          return type.name === typename;
        })
      )[0];
    }
  };
  DefaultTransformer.prototype.addFields = function(typename, fields) {
    var _this = this;
    var type = this.pluckType(typename);
    if (!this._config || !this._schema) {
      return;
    }
    if (type) {
      var typeConfig_1 = type.toConfig();
      Object.keys(fields).forEach(function(key) {
        var cfg = fields[key];
        var ReturnType = _this._schema.getType(cfg.type);
        var args = {};
        Object.keys(cfg.arguments || {}).forEach(function(argname) {
          var argcfg = cfg.arguments[argname];
          var ObjectType;
          if (typeof argcfg.type === "string") {
            ObjectType = _this._schema.getType(argcfg.type);
          }
          if (ObjectType) {
            var input = graphql_1.assertInputObjectType(ObjectType);
            var t = argcfg.required
              ? new graphql_1.GraphQLNonNull(input)
              : input;
            args[argname] = { type: t };
          } else {
            var input = graphql_1.assertInputType(argcfg.type);
            var t = argcfg.required
              ? new graphql_1.GraphQLNonNull(input)
              : input;
            args[argname] = { type: t };
          }
        });
        if (ReturnType) {
          var ListType = new graphql_1.GraphQLList(ReturnType);
          typeConfig_1.fields[key] = {
            type: cfg.list
              ? graphql_1.assertListType(ListType)
              : graphql_1.assertOutputType(ReturnType),
            args: args
          };
        }
      });
      if (typename === "Query") {
        this._config.query = new graphql_1.GraphQLObjectType(typeConfig_1);
      } else if (typename === "Mutation") {
        this._config.mutation = new graphql_1.GraphQLObjectType(typeConfig_1);
      } else if (typename === "Subscription") {
        this._config.subscription = new graphql_1.GraphQLObjectType(
          typeConfig_1
        );
      } else {
        this._config.types.push(new graphql_1.GraphQLObjectType(typeConfig_1));
      }
    }
  };
  return DefaultTransformer;
})();
exports.DefaultTransformer = DefaultTransformer;
