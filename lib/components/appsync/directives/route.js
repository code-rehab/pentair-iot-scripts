"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var appsync_data_1 = require("../appsync-data");
var RouteDirective = /** @class */ (function(_super) {
  __extends(RouteDirective, _super);
  function RouteDirective() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  RouteDirective.typeDef = function() {
    return "directive @route(to: String) on FIELD_DEFINITION";
  };
  RouteDirective.prototype.visitFieldDefinition = function(field, details) {
    var typeName = details.objectType.name;
    var fieldName = field.name;
    var template = { type: "route", use: "resolveBatch" };
    appsync_data_1.schemaData.mapping.insert(typeName + "." + fieldName, {
      template: template,
      route: this.args.to || typeName.toLowerCase() + "." + fieldName,
      field: field.name,
      type: details.objectType
    });
  };
  return RouteDirective;
})(graphql_tools_1.SchemaDirectiveVisitor);
exports.RouteDirective = RouteDirective;
