"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var appsync_data_1 = require("../../appsync-data");
var model_1 = require("../model");
var typeDefs = model_1.ModelDirective.typeDef();
test("@model should add routes to schemaData", function() {
  graphql_tools_1.makeExecutableSchema({
    typeDefs:
      typeDefs +
      "  \n      type User @model {\n        name: String,\n        description: String\n      }\n      ",
    schemaDirectives: {
      model: model_1.ModelDirective
    }
  });
  var result = appsync_data_1.schemaData.mapping.get();
  expect(JSON.stringify(result["Mutation.createUser"])).toBe(
    '{"route":"user.create","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
  expect(JSON.stringify(result["Mutation.deleteUser"])).toBe(
    '{"route":"user.delete","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
  expect(JSON.stringify(result["Mutation.updateUser"])).toBe(
    '{"route":"user.update","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
  expect(JSON.stringify(result["Query.User"])).toBe(
    '{"route":"user.get","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
  expect(JSON.stringify(result["Query.UserCollection"])).toBe(
    '{"route":"user.list","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
});
