"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var appsync_data_1 = require("../../appsync-data");
var route_1 = require("../route");
var typeDefs = route_1.RouteDirective.typeDef();
test("@route should add a route to schemaData", function() {
  graphql_tools_1.makeExecutableSchema({
    typeDefs:
      typeDefs +
      "  \n      type User {\n        name: String,\n        description: String @route\n        someList: [String] @route\n      }\n      ",
    schemaDirectives: {
      route: route_1.RouteDirective
    }
  });
  var result = appsync_data_1.schemaData.mapping.get();
  expect(JSON.stringify(result["User.description"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"user.description","field":"description","type":"User"}'
  );
  expect(JSON.stringify(result["User.someList"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"user.someList","field":"someList","type":"User"}'
  );
});
test("@route(to:String) should add a route to schemaData with the defined route", function() {
  graphql_tools_1.makeExecutableSchema({
    typeDefs:
      typeDefs +
      '  \n      type User {\n        name: String,\n        description: String @route(to:"some.route.name")\n      }\n      ',
    schemaDirectives: {
      route: route_1.RouteDirective
    }
  });
  var result = appsync_data_1.schemaData.mapping.get();
  expect(JSON.stringify(result["User.description"])).toBe(
    '{"template":{"type":"route","use":"resolveBatch"},"route":"some.route.name","field":"description","type":"User"}'
  );
});
