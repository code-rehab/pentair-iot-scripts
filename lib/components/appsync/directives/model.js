"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var graphql_1 = require("graphql");
var appsync_data_1 = require("../appsync-data");
var transformer_1 = require("../classes/transformer");
var ResourceFunction;
(function(ResourceFunction) {
  ResourceFunction["Get"] = "get";
  ResourceFunction["List"] = "list";
  ResourceFunction["Create"] = "create";
  ResourceFunction["Update"] = "update";
  ResourceFunction["Delete"] = "delete";
})(ResourceFunction || (ResourceFunction = {}));
var ModelDirective = /** @class */ (function(_super) {
  __extends(ModelDirective, _super);
  function ModelDirective() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  ModelDirective.typeDef = function() {
    return "directive @model(queries: String) on OBJECT";
  };
  ModelDirective.transformer = function() {
    return new ModelTransformer();
  };
  ModelDirective.prototype.createRoute = function(functionName, type) {
    return {
      route: type.name.toLowerCase() + "." + functionName,
      template: { type: "route", use: "resolve" },
      field: "",
      type: type
    };
  };
  ModelDirective.prototype.visitObject = function(object) {
    var name = object.name;
    appsync_data_1.schemaData.models.insert(name, object);
    appsync_data_1.schemaData.mapping
      .insert("Query." + name, this.createRoute(ResourceFunction.Get, object))
      .insert(
        "Query." + name + "Collection",
        this.createRoute(ResourceFunction.List, object)
      )
      .insert(
        "Mutation.create" + name,
        this.createRoute(ResourceFunction.Create, object)
      )
      .insert(
        "Mutation.update" + name,
        this.createRoute(ResourceFunction.Update, object)
      )
      .insert(
        "Mutation.delete" + name,
        this.createRoute(ResourceFunction.Delete, object)
      );
  };
  return ModelDirective;
})(graphql_tools_1.SchemaDirectiveVisitor);
exports.ModelDirective = ModelDirective;
var ModelTransformer = /** @class */ (function(_super) {
  __extends(ModelTransformer, _super);
  function ModelTransformer() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  ModelTransformer.prototype.transformSchema = function(schema) {
    var _this = this;
    this._schema = schema;
    this._config = schema.toConfig();
    var mutationFields = {};
    var queryFields = {};
    appsync_data_1.schemaData.models.toArray().forEach(function(model) {
      var _a;
      var name = model.name;
      if (!_this._config.query.getFields()[name]) {
        queryFields[name] = {
          type: name,
          arguments: { id: { type: graphql_1.GraphQLID, required: true } }
        };
      }
      if (!_this._config.query.getFields()["all" + name + "s"]) {
        queryFields[name + "Collection"] = { type: name, list: true };
      }
      mutationFields = __assign(
        ((_a = {}),
        (_a["create" + name] = {
          type: name,
          arguments: { input: { type: "input" + name, required: true } }
        }),
        (_a["update" + name] = {
          type: name,
          arguments: { input: { type: "input" + name, required: true } }
        }),
        (_a["delete" + name] = {
          type: name,
          arguments: { id: { type: graphql_1.GraphQLID, required: true } }
        }),
        _a),
        mutationFields
      );
    });
    this.addFields("Mutation", mutationFields);
    this.addFields("Query", queryFields);
    return new graphql_1.GraphQLSchema(this._config);
  };
  return ModelTransformer;
})(transformer_1.DefaultTransformer);
