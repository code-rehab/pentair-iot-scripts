import { AppsyncConfig } from "../../config/aws";
import { AWSContainer } from "../aws/aws";
import { AppsyncRouteConfig } from "./appsync-data";
export declare const mapping: any;
export declare type AppsyncTemplateFunction = Record<string, (params: AppsyncRouteConfig) => string>;
export declare type AppsyncTemplate = Record<"response" | "request", AppsyncTemplateFunction>;
export declare type AppsyncTemplates = Record<string, AppsyncTemplate>;
export declare class AWS_Appsync {
    private _aws?;
    config: AppsyncConfig;
    private _extractedSubscriptionsBlock;
    private _extractedSubscriptions;
    private _subscriptionRegex;
    private _templates;
    constructor(_aws?: AWSContainer | undefined, config?: Partial<AppsyncConfig>);
    build(config?: Partial<AppsyncConfig>): Promise<void>;
    createConfig: (config?: Partial<AppsyncConfig>) => {
        schema: string;
        build_path: string;
    };
    private _formatFileName;
    generateMaps(): Promise<{
        dataSource: string;
        type: string;
        field: string;
        request: string;
        response: string;
    }[]>;
    generateTemplates(cfg: AppsyncRouteConfig): {
        request: string;
        response: string;
    };
    createSchema(_schema?: string): string;
    appendSubscriptions: (typeDefs: string) => string;
}
