"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.route_templates = {
  request: {
    resolve: function(_a) {
      var route = _a.route;
      return (
        '\n    #**\n      The value of \'payload\' after the template has been evaluated\n      will be passed as the event to AWS Lambda.\n    *#\n    {\n        "version" : "2017-02-28",\n        "operation": "Invoke",\n        "payload": {\n            "route": "' +
        route +
        '",\n            "arguments": $utils.toJson($context.arguments),\n            "identity": $utils.toJson($context.identity)\n        }\n    }'
      );
    },
    resolveBatch: function(_a) {
      var route = _a.route,
        field = _a.field;
      return (
        '\n    #**\n      The value of \'payload\' after the template has been evaluated\n      will be passed as the event to AWS Lambda.\n    *#\n    {\n        "version" : "2017-02-28",\n        "operation": "BatchInvoke",\n        "payload": {\n            "route": "' +
        route +
        '",\n            "field": "' +
        field +
        '",\n            "source": $utils.toJson($context.source),\n            "arguments": $utils.toJson($context.arguments),\n            "identity": $utils.toJson($context.identity)\n        }\n    }'
      );
    }
  },
  response: {
    resolve: function() {
      return "$utils.toJson($context.result)";
    },
    resolveBatch: function() {
      return "$utils.toJson($context.result)";
    }
  }
};
