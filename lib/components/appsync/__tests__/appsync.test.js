"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appsync_1 = require("../appsync");
var logger_1 = require("../../../classes/logger");
var graphql_1 = require("graphql");
logger_1.logger.silent = true;
describe("Appsync tests", function() {
  test("Should return a valid appsync schema", function() {
    var appsync = new appsync_1.AWS_Appsync();
    var schema = appsync.createSchema(
      '\n    # import * from "pentair"\n\n    input inputUser {\n      name: String\n      description: String\n    }\n\n    type User @model {\n      name: String\n      description: String @route(to:"some.route") \n    }\n \n    type Query{ \n      User(id:ID!): User \n    }\n    type Mutation\n    type Subscription {\n      subscriptionToExtract: User @aws_subscribe(mutations: ["updateUser"])\n    }\n  '
    );
    expect(schema.replace(/\s/g, "")).toBe(
      '\n     input inputUser {\n       name: String\n       description: String\n     }\n\n     type Mutation {\n       createUser(input: inputUser!): User\n       updateUser(input: inputUser!): User\n       deleteUser(id: ID!): User\n     } \n\n     type Query {\n       User(id: ID!): User\n       UserCollection: [User]\n     }\n\n     type Subscription {\n       subscriptionToExtract: User @aws_subscribe(mutations: ["updateUser"])\n       userUpdated: User @aws_subscribe(mutations: ["updateUser"])\n       userDeleted: User @aws_subscribe(mutations: ["deleteUser"])\n       userCreated: User @aws_subscribe(mutations: ["createUser"])\n     }\n\n     type User {\n       name: String\n       description: String\n     }'.replace(
        /\s/g,
        ""
      )
    );
  });
  test("Should be able to generate correct templates", function() {
    var appsync = new appsync_1.AWS_Appsync();
    var resolve = appsync.generateTemplates({
      template: { type: "route", use: "resolve" },
      field: "SomeField",
      route: "some.route",
      type: new graphql_1.GraphQLObjectType({
        name: "SomeType",
        fields: {}
      })
    });
    var resolveBatch = appsync.generateTemplates({
      template: { type: "route", use: "resolveBatch" },
      field: "SomeField",
      route: "some.route",
      type: new graphql_1.GraphQLObjectType({
        name: "SomeType",
        fields: {}
      })
    });
    expect(resolve).toEqual({
      request:
        '\n    #**\n      The value of \'payload\' after the template has been evaluated\n      will be passed as the event to AWS Lambda.\n    *#\n    {\n        "version" : "2017-02-28",\n        "operation": "Invoke",\n        "payload": {\n            "route": "some.route",\n            "arguments": $utils.toJson($context.arguments),\n            "identity": $utils.toJson($context.identity)\n        }\n    }',
      response: "$utils.toJson($context.result)"
    });
    expect(resolveBatch).toEqual({
      request:
        '\n    #**\n      The value of \'payload\' after the template has been evaluated\n      will be passed as the event to AWS Lambda.\n    *#\n    {\n        "version" : "2017-02-28",\n        "operation": "BatchInvoke",\n        "payload": {\n            "route": "some.route",\n            "field": "SomeField",\n            "source": $utils.toJson($context.source),\n            "arguments": $utils.toJson($context.arguments),\n            "identity": $utils.toJson($context.identity)\n        }\n    }',
      response: "$utils.toJson($context.result)"
    });
  });
});
