import { GraphQLObjectType, GraphQLInterfaceType } from "graphql";
export declare type AppsyncRouteMapping = Record<string, AppsyncRouteConfig>;
export declare class AppsyncObjectController<T> implements IAppsyncObjectController<T> {
    private _data;
    get(): Record<string, T>;
    keys(): string[];
    toArray(): T[];
    insert(key: string, value: T): this;
}
declare class AppsyncSchemaData {
    mapping: AppsyncMappingController;
    models: AppsyncModelsController;
}
export interface IAppsyncObjectController<T> {
    insert(key: string, value: T): AppsyncObjectController<T>;
    keys(): string[];
    toArray(): T[];
    get(): Record<string, T>;
}
export interface AppsyncRouteConfig {
    route: string;
    template: {
        type: string;
        use: string;
    };
    type: GraphQLObjectType | GraphQLInterfaceType;
    field: string;
}
export declare type AppsyncMappingController = IAppsyncObjectController<AppsyncRouteConfig>;
export declare type AppsyncModelsController = IAppsyncObjectController<GraphQLObjectType>;
export declare const schemaData: AppsyncSchemaData;
export {};
