"use strict";
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var fs_extra_1 = require("fs-extra");
var graphql_import_1 = require("graphql-import");
var graphql_tools_1 = require("graphql-tools");
var logger_1 = require("../../classes/logger");
var aws_1 = require("../../config/aws");
var graphql_1 = require("graphql");
var model_1 = require("./directives/model");
var route_1 = require("./directives/route");
var appsync_data_1 = require("./appsync-data");
var route_2 = require("./templates/route");
exports.mapping = [];
var AWS_Appsync = /** @class */ (function() {
  function AWS_Appsync(_aws, config) {
    var _this = this;
    if (config === void 0) {
      config = {};
    }
    this._aws = _aws;
    this._extractedSubscriptionsBlock = "";
    this._extractedSubscriptions = "";
    this._subscriptionRegex = /type Subscription \{([^}]+)\}/gm;
    this._templates = {
      route: route_2.route_templates
    };
    this.createConfig = function(config) {
      if (config === void 0) {
        config = {};
      }
      return __assign({}, aws_1.aws_config.appsync, config);
    };
    this.appendSubscriptions = function(typeDefs) {
      var subscriptions = _this._extractedSubscriptions.trim();
      appsync_data_1.schemaData.models.toArray().forEach(function(model) {
        var typename = model.name;
        subscriptions +=
          "\n  " +
          typename.toLowerCase() +
          "Updated: " +
          typename +
          ' @aws_subscribe(mutations: ["update' +
          typename +
          '"])\n  ' +
          typename.toLowerCase() +
          "Deleted: " +
          typename +
          ' @aws_subscribe(mutations: ["delete' +
          typename +
          '"])\n  ' +
          typename.toLowerCase() +
          "Created: " +
          typename +
          ' @aws_subscribe(mutations: ["create' +
          typename +
          '"])\n';
      });
      var match = /type Subscription \{([^}]+)\}/gm.exec(typeDefs);
      return typeDefs.replace(
        (match && match[1]) || "",
        "\n  " + subscriptions
      );
    };
    this.config = this.createConfig(config);
  }
  AWS_Appsync.prototype.build = function(config) {
    if (config === void 0) {
      config = {};
    }
    return __awaiter(this, void 0, void 0, function() {
      var e_1;
      return __generator(this, function(_a) {
        switch (_a.label) {
          case 0:
            this.config = this.createConfig(config);
            _a.label = 1;
          case 1:
            _a.trys.push([1, 4, , 5]);
            return [4 /*yield*/, this.createSchema()];
          case 2:
            _a.sent();
            return [4 /*yield*/, this.generateMaps()];
          case 3:
            _a.sent();
            // TODO: Add function for Generating python routes.
            logger_1.logger.info(
              "Build: Finished compiling Appsync dependencies"
            );
            return [3 /*break*/, 5];
          case 4:
            e_1 = _a.sent();
            logger_1.logger.error(e_1);
            return [3 /*break*/, 5];
          case 5:
            return [2 /*return*/];
        }
      });
    });
  };
  AWS_Appsync.prototype._formatFileName = function(filename, suffix) {
    return (filename + suffix).toLowerCase().replace(".", "-");
  };
  AWS_Appsync.prototype.generateMaps = function() {
    return __awaiter(this, void 0, void 0, function() {
      var build_path,
        mapping_build_path,
        mapping,
        mapConfig,
        key,
        cfg,
        fields,
        requestFileName,
        responseFileName,
        templates;
      return __generator(this, function(_a) {
        logger_1.logger.info("Build: Generating Appsync resolver mappings");
        build_path = this.config.build_path;
        mapping_build_path = this.config.build_path + "mapping/";
        mapping = appsync_data_1.schemaData.mapping.get();
        fs_extra_1.emptyDirSync(mapping_build_path);
        mapConfig = [];
        for (key in mapping) {
          cfg = mapping[key];
          fields = key.split(".");
          requestFileName = this._formatFileName(key, "-request.txt");
          responseFileName = this._formatFileName(key, "-response.txt");
          templates = this.generateTemplates(cfg);
          fs_1.writeFileSync(
            mapping_build_path + requestFileName,
            templates.request
          );
          fs_1.writeFileSync(
            mapping_build_path + responseFileName,
            templates.response
          );
          mapConfig.push({
            dataSource: "AppSyncResolver",
            type: fields[0],
            field: fields[1],
            request: requestFileName,
            response: responseFileName
          });
        }
        fs_1.writeFileSync(
          build_path + "mapping.json",
          JSON.stringify(mapConfig)
        );
        return [2 /*return*/, mapConfig];
      });
    });
  };
  AWS_Appsync.prototype.generateTemplates = function(cfg) {
    var request = this._templates[cfg.template.type]["request"][
      cfg.template.use
    ];
    var response = this._templates[cfg.template.type]["response"][
      cfg.template.use
    ];
    return {
      request: request(cfg),
      response: response(cfg)
    };
  };
  AWS_Appsync.prototype.createSchema = function(_schema) {
    logger_1.logger.info("Build: Stitching Appsync schema");
    var root = " \n    schema {\n      mutation: Mutation\n      query: Query\n      subscription: Subscription\n    }".trim();
    var pentair =
      "\n    " +
      model_1.ModelDirective.typeDef() +
      "\n    " +
      route_1.RouteDirective.typeDef() +
      "\n\n    scalar AWSJSON\n    directive @aws_subscribe(mutations: [String!]!) on FIELD_DEFINITION";
    var schema = graphql_import_1.importSchema(_schema || this.config.schema, {
      pentair: pentair
    });
    var typeDefs = root + "\n\n" + schema;
    var executableSchema = graphql_tools_1.makeExecutableSchema({
      typeDefs: typeDefs,
      schemaDirectives: {
        route: route_1.RouteDirective,
        model: model_1.ModelDirective
      }
    });
    var match = this._subscriptionRegex.exec(typeDefs);
    this._extractedSubscriptions = (match && match[1]) || "";
    var buildPath = this.config.build_path + "schema.graphql";
    var transformedSchema = graphql_tools_1.transformSchema(executableSchema, [
      model_1.ModelDirective.transformer()
    ]);
    var finalSchema = graphql_1.printSchema(transformedSchema);
    finalSchema = finalSchema.replace(
      model_1.ModelDirective.typeDef() + "\n",
      ""
    );
    finalSchema = finalSchema.replace(
      route_1.RouteDirective.typeDef() + "\n",
      ""
    );
    finalSchema = finalSchema.replace("scalar AWSJSON", "");
    finalSchema = finalSchema.replace("scalar AWSJSON", "");
    finalSchema = this.appendSubscriptions(finalSchema);
    finalSchema = finalSchema.trim();
    try {
      fs_1.writeFileSync(buildPath, finalSchema);
    } catch (e) {
      logger_1.logger.error("Unable to write schema to: " + buildPath);
    }
    return finalSchema;
  };
  return AWS_Appsync;
})();
exports.AWS_Appsync = AWS_Appsync;
