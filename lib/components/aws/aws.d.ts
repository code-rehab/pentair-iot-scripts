import { AWS_Appsync } from "../appsync/appsync";
import { AWS_Cognito } from "./cognito";
import { AWS_S3 } from "./s3";
export declare class AWSContainer {
    private _config;
    private _s3;
    private _cognito;
    private _appsync;
    region: string;
    constructor(region?: string, profile?: string);
    readonly s3: AWS_S3;
    readonly cognito: AWS_Cognito;
    readonly appsync: AWS_Appsync;
    getCFValue: (key: string) => Promise<unknown>;
}
