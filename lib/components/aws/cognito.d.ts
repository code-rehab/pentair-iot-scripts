import { AWSContainer } from "./aws";
export interface UserConfig {
    username: string;
    group?: string;
    accessID?: string;
    attributes?: Record<string, string>;
}
export declare class AWS_Cognito {
    private _aws;
    private _temporaryPass;
    private _finalPass;
    private _pooldata;
    private _serviceProvider;
    constructor(_aws: AWSContainer);
    private _pool;
    createUsers(config: any): Promise<void>;
    removeUsers(config: any): Promise<void>;
    _removeUsers(config: any, pool?: any): Promise<[unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown]>;
    private _removeUser;
    private _createUser;
    addUserToGroup(user: UserConfig, pool: any): Promise<unknown>;
    getUserToken(user: UserConfig, pool: any, callback: (data: string) => any): Promise<void>;
    resetUserPassword(user: any, pool: any): Promise<unknown>;
}
