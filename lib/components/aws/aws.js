"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = require("aws-sdk");
var logger_1 = require("../../classes/logger");
var appsync_1 = require("../appsync/appsync");
var cognito_1 = require("./cognito");
var s3_1 = require("./s3");
aws_sdk_1.config.update({
  credentials: new aws_sdk_1.SharedIniFileCredentials()
});
var AWSContainer = /** @class */ (function() {
  function AWSContainer(region, profile) {
    var _this = this;
    if (region === void 0) {
      region = "us-east-1";
    }
    if (profile === void 0) {
      profile = "personal";
    }
    this._config = {};
    this._s3 = undefined;
    this._cognito = undefined;
    this._appsync = undefined;
    this.region = "us-east-1";
    // get value from Cloudformation stacks
    this.getCFValue = function(key) {
      return __awaiter(_this, void 0, void 0, function() {
        return __generator(this, function(_a) {
          return [
            2 /*return*/,
            new Promise(function(resolve, reject) {
              var cfn = new aws_sdk_1.CloudFormation();
              cfn.listExports({}, function(error, data) {
                if (error) {
                  logger_1.logger.error(
                    "\n            Error during AWS.CloudFormation.listExports ! " +
                      error.message +
                      "\n            Please check the cloudformation documentation at: https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/documentation/browse/HEAD/--/cloudformation/CrossAccountAwsCli.md\n            "
                  );
                  process.exit(1);
                }
                var result =
                  data &&
                  data.Exports &&
                  data.Exports.filter(function(_export) {
                    return _export.Name === key;
                  });
                // check if the export exists
                if (!result || result.length !== 1) {
                  reject(
                    "Cloudformation export " +
                      key +
                      " not found, Please initialize your account before deploying this package"
                  );
                } else {
                  resolve(result[0].Value);
                }
              });
            })
          ];
        });
      });
    };
    aws_sdk_1.config.update({ region: region });
    this.region = region;
  }
  Object.defineProperty(AWSContainer.prototype, "s3", {
    // Create or get an AWS_S3 instance
    get: function() {
      if (!this._s3) {
        this._s3 = new s3_1.AWS_S3(this);
      }
      return this._s3;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(AWSContainer.prototype, "cognito", {
    // Create or get an AWS_Cognito instance
    get: function() {
      if (!this._cognito) {
        this._cognito = new cognito_1.AWS_Cognito(this);
      }
      return this._cognito;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(AWSContainer.prototype, "appsync", {
    // Create or get an AWS_Cognito instance
    get: function() {
      if (!this._appsync) {
        this._appsync = new appsync_1.AWS_Appsync(this);
      }
      return this._appsync;
    },
    enumerable: true,
    configurable: true
  });
  return AWSContainer;
})();
exports.AWSContainer = AWSContainer;
