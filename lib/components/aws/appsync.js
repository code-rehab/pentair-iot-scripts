"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var fs_extra_1 = require("fs-extra");
var graphql_import_1 = require("graphql-import");
var graphql_tools_1 = require("graphql-tools");
var logger_1 = require("../../classes/logger");
var aws_1 = require("../../config/aws");
var graphql_1 = require("graphql");
var mapping = [];
var schemaRoot = "schema {\n  mutation: Mutation\n  query: Query\n  subscription: Subscription\n}".trim();
var AWS_Appsync = /** @class */ (function() {
  function AWS_Appsync(_aws, config) {
    var _this = this;
    if (config === void 0) {
      config = {};
    }
    this._aws = _aws;
    this._templates = {
      request: {
        default: function(_a) {
          var route = _a.route;
          return (
            '\n      #**\n        The value of \'payload\' after the template has been evaluated\n        will be passed as the event to AWS Lambda.\n      *#\n      {\n          "version" : "2017-02-28",\n          "operation": "Invoke",\n          "payload": {\n              "route": "' +
            route +
            '",\n              "arguments": $utils.toJson($context.arguments),\n              "identity": $utils.toJson($context.identity)\n          }\n      }'
          );
        }
      },
      response: {
        default: function() {
          return "$utils.toJson($context.result)";
        }
      }
    };
    this.updateConfig = function(_config) {
      if (_config === void 0) {
        _config = {};
      }
      var config = __assign({}, aws_1.aws_config.appsync, _config);
      // this._templates.request = {
      //   ...require(config.templates.request)
      // };
      // this._templates.response = {
      //   ...require(config.templates.response)
      // };
      _this._config = config;
      return config;
    };
    this._config = this.updateConfig(config);
  }
  AWS_Appsync.prototype.build = function(config) {
    if (config === void 0) {
      config = {};
    }
    return __awaiter(this, void 0, void 0, function() {
      var maps, schema, e_1;
      return __generator(this, function(_a) {
        switch (_a.label) {
          case 0:
            this.updateConfig(config);
            _a.label = 1;
          case 1:
            _a.trys.push([1, 4, , 5]);
            return [4 /*yield*/, this._generateMaps()];
          case 2:
            maps = _a.sent();
            return [4 /*yield*/, this.createSchema()];
          case 3:
            schema = _a.sent();
            logger_1.logger.info(
              "Build: Finished compiling Appsync dependencies"
            );
            return [3 /*break*/, 5];
          case 4:
            e_1 = _a.sent();
            logger_1.logger.error(e_1);
            return [3 /*break*/, 5];
          case 5:
            return [2 /*return*/];
        }
      });
    });
  };
  AWS_Appsync.prototype.template = function(type, select) {
    return this._templates[type][select] || this._templates[type].default;
  };
  AWS_Appsync.prototype._formatFileName = function(filename, suffix) {
    return (filename + suffix).toLowerCase().replace(".", "-");
  };
  AWS_Appsync.prototype._generateMaps = function() {
    return __awaiter(this, void 0, void 0, function() {
      var build_path,
        mapping_build_path,
        configJson,
        mapConfig,
        key,
        cfg,
        fields,
        requestFileName,
        responseFileName,
        request,
        response,
        params;
      return __generator(this, function(_a) {
        logger_1.logger.info("Build: Generating Appsync resolver mappings");
        build_path = this._config.build_path;
        mapping_build_path = this._config.build_path + "mapping/";
        configJson = JSON.parse(
          fs_1.readFileSync(this._config.mapping, "utf8")
        );
        fs_extra_1.emptyDirSync(mapping_build_path);
        mapConfig = [];
        for (key in configJson) {
          cfg = configJson[key];
          fields = key.split(".");
          requestFileName = this._formatFileName(key, "-request.txt");
          responseFileName = this._formatFileName(key, "-response.txt");
          request = this.template("request", cfg.request);
          response = this.template("response", cfg.response);
          params = __assign(
            { type: fields[0], field: fields[1], route: cfg.route },
            cfg.params || {}
          );
          fs_1.writeFileSync(
            mapping_build_path + requestFileName,
            request(params)
          );
          fs_1.writeFileSync(
            mapping_build_path + responseFileName,
            response(params)
          );
          mapConfig.push({
            dataSource: cfg.resolver || "AppSyncResolver",
            type: fields[0],
            field: fields[1],
            request: requestFileName,
            response: responseFileName
          });
        }
        fs_1.writeFileSync(
          build_path + "mapping.json",
          JSON.stringify(mapConfig)
        );
        return [2 /*return*/, mapConfig];
      });
    });
  };
  AWS_Appsync.prototype.createSchema = function(schema) {
    if (schema === void 0) {
      schema = "";
    }
    logger_1.logger.info("Build: Stitching Appsync schema");
    var typeDefs = schemaRoot + "\n\n" + graphql_import_1.importSchema(schema);
    var schemaDirectives = { route: RouteDirective, model: ModelDirective };
    var execSchema = graphql_tools_1.makeExecutableSchema({
      typeDefs: typeDefs,
      schemaDirectives: schemaDirectives
    });
    console.log(mapping);
    // typeDefs = typeDefs.replace("scalar AWSJSON", "");
    // writeFileSync(this._config.build_path + "schema.graphql", typeDefs);
    return graphql_1.printSchema(execSchema);
  };
  return AWS_Appsync;
})();
exports.AWS_Appsync = AWS_Appsync;
var ModelDirective = /** @class */ (function(_super) {
  __extends(ModelDirective, _super);
  function ModelDirective() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  ModelDirective.prototype.visitObject = function(object) {
    var name = object.name;
    mapping["Query." + name] = { route: name.toLowerCase() + ".get" };
    mapping["Query.all" + name + "s"] = { route: name.toLowerCase() + ".list" };
    mapping["Mutation.create" + name] = {
      route: name.toLowerCase() + ".create"
    };
    mapping["Mutation.update" + name] = {
      route: name.toLowerCase() + ".update"
    };
    mapping["Mutation.delete" + name] = {
      route: name.toLowerCase() + ".delete"
    };
  };
  return ModelDirective;
})(graphql_tools_1.SchemaDirectiveVisitor);
var RouteDirective = /** @class */ (function(_super) {
  __extends(RouteDirective, _super);
  function RouteDirective() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  RouteDirective.prototype.visitFieldDefinition = function() {
    console.log("visited");
  };
  return RouteDirective;
})(graphql_tools_1.SchemaDirectiveVisitor);
