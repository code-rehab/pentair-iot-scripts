"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
var __importDefault =
  (this && this.__importDefault) ||
  function(mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
var archiver_1 = __importDefault(require("archiver"));
var child_process_1 = require("child_process");
var fs_extra_1 = require("fs-extra");
var logger_1 = require("../classes/logger");
var defaults_1 = require("../config/defaults");
var Python = /** @class */ (function() {
  function Python(config) {
    this._config = {};
    // this.config = cfg.create(config);
  }
  Python.prototype.buildDev = function(source, destination, artifact_name) {
    if (destination === void 0) {
      destination = defaults_1.build_path + "python/";
    }
    if (artifact_name === void 0) {
      artifact_name = "lambda";
    }
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        switch (_a.label) {
          case 0:
            // destination = destination || this.config.build_path;
            return [4 /*yield*/, this.build_source(source, destination)];
          case 1:
            // destination = destination || this.config.build_path;
            _a.sent();
            return [
              4 /*yield*/,
              this.create_artifact(destination, artifact_name)
            ];
          case 2:
            _a.sent();
            return [2 /*return*/];
        }
      });
    });
  };
  Python.prototype.build = function(source, destination, name, dependencies) {
    if (destination === void 0) {
      destination = defaults_1.build_path + "python/";
    }
    if (name === void 0) {
      name = "lambda";
    }
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        switch (_a.label) {
          case 0:
            fs_extra_1.removeSync(destination + name);
            if (!dependencies) return [3 /*break*/, 2];
            return [4 /*yield*/, this.build_dependencies(destination + name)];
          case 1:
            _a.sent();
            logger_1.logger.info(
              "Build: Finished building dependencies for: " + name
            );
            _a.label = 2;
          case 2:
            if (!source) return [3 /*break*/, 5];
            return [4 /*yield*/, this.build_source(source, destination + name)];
          case 3:
            _a.sent();
            return [
              4 /*yield*/,
              this.create_artifact(destination + name, name)
            ];
          case 4:
            _a.sent();
            logger_1.logger.info(
              "Build: Finished building source for: " + name
            );
            _a.label = 5;
          case 5:
            return [2 /*return*/];
        }
      });
    });
  };
  Python.prototype.format = function() {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        logger_1.logger.info("Formatting Python files...");
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            var child = child_process_1.spawn("pipenv run black .*/**/*.py", {
              shell: "/bin/bash"
            });
            child.on("close", function(code) {
              if (code !== 0) {
                logger_1.logger.error(
                  "Command failed with exit code " + code + "."
                );
                process.exit(1);
              }
              resolve();
            });
            // print live stdout data to the console
            child.stdout.on("data", function(data) {
              process.stdout.write("" + data);
            });
            // print live stderr data to the console
            child.stderr.on("data", function(data) {
              logger_1.logger.error("" + data);
            });
          })
        ];
      });
    });
  };
  Python.prototype.test_source = function() {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        logger_1.logger.info("Running Python tests...");
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            var child = child_process_1.spawn("pipenv run pytest", {
              shell: "/bin/bash"
            });
            child.on("close", function(code) {
              if (code !== 0) {
                logger_1.logger.error(
                  "Command failed with exit code " + code + "."
                );
                process.exit(1);
              }
              resolve();
            });
            // print live stdout data to the console
            child.stdout.on("data", function(data) {
              process.stdout.write("" + data);
            });
            // print live stderr data to the console
            child.stderr.on("data", function(data) {
              logger_1.logger.error("" + data);
            });
          })
        ];
      });
    });
  };
  Python.prototype.build_dependencies = function(destination) {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        logger_1.logger.info("Build: Fetching python dependencies...");
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            var child = child_process_1.spawn(
              "pip install --upgrade -r <(pipenv lock -r) --target " +
                destination,
              { shell: "/bin/bash" }
            );
            child.on("close", function(code) {
              if (code !== 0) {
                logger_1.logger.error(
                  "Command failed with exit code " + code + "."
                );
                process.exit(1);
              }
              logger_1.logger.info(
                "Build: Python dependencies copied to " + destination
              );
              resolve();
            });
            // print live stdout data to the console
            child.stdout.on("data", function(data) {
              process.stdout.write("" + data);
            });
            // print live stderr data to the console
            child.stderr.on("data", function(data) {
              process.stdout.write("" + data);
            });
          })
        ];
      });
    });
  };
  Python.prototype.build_source = function(source, destination) {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        logger_1.logger.info("Build: Copying source files...");
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            fs_extra_1
              .copy(source, destination)
              .then(function() {
                logger_1.logger.info("Build: Successfully copied files");
                resolve();
              })
              .catch(function(err) {
                logger_1.logger.error("Build: Error copying files: " + err);
                process.exit(1);
                reject();
              });
          })
        ];
      });
    });
  };
  Python.prototype.create_artifact = function(source, name) {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        logger_1.logger.info("Build: Creating artifact...");
        if (!fs_extra_1.existsSync(".build/artifacts")) {
          fs_extra_1.mkdirSync(".build/artifacts");
        }
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            var output = fs_extra_1.createWriteStream(
              ".build/artifacts/" + name + ".zip"
            );
            var archive = archiver_1.default("zip", {
              zlib: { level: 9 } // Sets the compression level.
            });
            output.on("close", function() {
              logger_1.logger.info(
                "Build: Successfully created artifact " + name
              );
              resolve();
            });
            archive.on("error", function(err) {
              reject(err);
            });
            archive.directory(source, false);
            archive.pipe(output);
            archive.finalize();
          })
        ];
      });
    });
  };
  return Python;
})();
exports.Python = Python;
