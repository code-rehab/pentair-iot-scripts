import { ShellCommand, ShellCommands } from "../classes/command-executor";
export interface AWSCommands {
    s3: ShellCommands<{
        pull: ShellCommand;
        push: ShellCommand;
    }>;
    cognito: ShellCommands<{
        createUsers: ShellCommand;
        removeUsers: ShellCommand;
    }>;
    appsync: ShellCommands<{
        build: ShellCommand;
    }>;
}
export declare function awsCommands(): AWSCommands;
